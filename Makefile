.PHONY: all run clean

ASM = nasm
ASMFLAGS = -f elf32
CC = clang
CFLAGS = -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector \
		-nostartfiles -nodefaultlibs -Wall -Wextra -Werror -I.
LD = ld.lld
LDFLAGS = -T link.ld -melf_i386
QEMU = qemu-system-i386

CSRC = kmain.c fbio.c memory.c
COBJ = $(CSRC:.c=.o)
CDEP = $(CSRC:.c=.d)
ASMSRC = loader.s
ASMOBJ = $(ASMSRC:.s=.o)
KERNEL = kernel.elf

include $(CDEP)

all: $(KERNEL)

run: $(KERNEL)
	$(QEMU) -kernel $(KERNEL)

$(KERNEL): $(ASMOBJ) $(COBJ)
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.s
	$(ASM) $(ASMFLAGS) $<

%.d: %.c
	$(CC) $(CFLAGS) -E -MM -MT $(subst .d,.o,$@) $< >$@

clean:
	rm -rf $(KERNEL) $(ASMOBJ) $(COBJ) $(CDEP)
