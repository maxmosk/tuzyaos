%include "kmain.inc"

global kstart

MAGIC_NUMBER equ 0x1BADB002
FLAGS        equ 0x0
CHECKSUM     equ -MAGIC_NUMBER

STACK_SIZE   equ 4 * 1024

section .multiboot
align 4
    dd MAGIC_NUMBER
    dd FLAGS
    dd CHECKSUM

section .text
align 4
kstart:
    mov     esp,    stack
    call    kmain
.endless_loop:
    jmp     .endless_loop

section .bss
align 4
stack:
    resb STACK_SIZE
