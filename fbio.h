#ifndef FBIO_H_INCLUDED
#define FBIO_H_INCLUDED

#define FB_START ((struct fb_line *)0x000B8000)
#define FB_WIDTH 80
#define FB_HEIGHT 25

enum fb_color
{
    FB_BLACK =         0,
    FB_BLUE  =         1,
    FB_GREEN =         2,
    FB_CYAN  =         3,
    FB_RED   =         4,
    FB_MAGENTA =       5,
    FB_BROWN =         6,
    FB_LIGHT_GRAY =    7,
    FB_DARK_GREY =     8,
    FB_LIGHT_BLUE =    9,
    FB_LIGHT_GREEN =   10,
    FB_LIGHT_CYAN =    11,
    FB_LIGHT_RED =     12,
    FB_LIGHT_MAGENTA = 13,
    FB_LIGHT_BROWN =   14,
    FB_WHITE =         15
};

struct fb_char
{
    char ascii;
    unsigned fg:4;
    unsigned bg:4;
} __attribute__((packed));

typedef struct fb_char fb_line[FB_WIDTH];

int fb_write(const char *string);

#endif /* FBIO_H_INCLUDED */
